# 0.1) install go if not yet installed (relevant for macOS): https://go.dev/doc/install
# 0.2) install git if not yet installed (https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

# 1) install Terraform by compiling the binary
git clone https://github.com/hashicorp/terraform.git --branch v1.5.1 # note: we are somewhat depending on the version which is used in the gitlab-terraform image, because it is used in the CI/CD pipelines. That's why a specific release is installed here.
cd terraform
go install # it should be automatically added to the $GOPATH, which should be part of the $PATH... If not, change the path manually to include the terraform binary location (wherever it may be)
terraform -v # please verify the installation

# 2) install kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.14.9/bin/linux/amd64/kubectl
mv kubectl /usr/bin/kubectl && chmod +x /usr/bin/kubectl # linux
# mv kubectl /usr/local/bin/kubectl $$ chmod +x /usr/local/bin/kubectl # macOS

# 3) generate an ssh key and manually add it to GitLab via the UI, to clone any modules...
# ssh-keygen

# 4) generate a personal access token in Gitlab, export as local environment variable. This will be necessary to access any Terraform state, which is stored on Gitlab.
export GITLAB_ACCESS_TOKEN=<YOUR-ACCESS-TOKEN>

# 5) [project-specific] export other relevant variables. 
